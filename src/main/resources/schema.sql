drop table if exists tasks_devs;
drop table if exists tasks;
drop table if exists devs;


create table devs (
      id  bigserial not null,
      dev_name varchar(255) not null,
      user_name varchar(255) not null,
      pwd varchar(255),
      primary key (id)
);

create table tasks (
       id  bigserial not null,
       title varchar(255) not null,
       description varchar(255) not null,
       due_date Date not null,
       done boolean not null,
       primary key (id)
);

create table tasks_devs (
        dev_id int8 not null,
        task_id int8 not null,
        primary key (dev_id, task_id)
);
alter table tasks_devs add constraint FK_tasks foreign key (task_id) references tasks;
alter table tasks_devs add constraint FK_devs foreign key (dev_id) references devs;

-- Insert admin user - Name: admin, userName: simpleApiAdmin and password: admin_@simpleApi
INSERT INTO public.devs (dev_name, user_name, pwd) VALUES ('admin', 'simpleApiAdmin', '$2a$10$18cAbuGDrjf0lK2Nb9/ubuxwUe3Mefp5lR3R73PMocnTDf06ek.rq');
INSERT INTO public.tasks (title, description, due_date, done) VALUES('Task 1', 'task 1 description', '2021-10-21', false);
INSERT INTO public.tasks (title, description, due_date, done) VALUES('Task 2', 'task 2 description', '2021-10-22', false);
INSERT INTO public.tasks (title, description, due_date, done) VALUES('Task 3', 'task 3 description', '2021-10-23', false);

