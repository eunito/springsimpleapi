package com.repo.training.simpleApi.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "Devs")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id") //prevents recursions
public class Dev implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Dev's name must not be null")
    @Length(min = 3, message = "Dev's name must have more than 3 chars")
    private String devName;

    @NotNull(message = "Dev's username must not be null")
    @Length(min = 3, message = "Dev's username must have more than 3 chars")
    private String userName;

    @NotNull(message = "Password must not be null")
    @Length(min = 5, message = "Password must have more than 5 chars")
    private String pwd;

    // Assuming that a task can have many devs and vice versa
    @RestResource(path = "/assocTask")
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "tasks_devs",
            joinColumns = @JoinColumn(name = "dev_id",  referencedColumnName = "id",nullable = false, updatable = false),
            inverseJoinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id", nullable = false, updatable = false))
    private List<Task> tasks = new ArrayList<>();

    public Dev() {
    }

    public Dev(String name, String password) {
        this.devName = name;
        this.pwd = password;
    }

    public String getName() {
        return devName;
    }

    public void setName(String name) {
        this.devName = name;
    }

    @JsonIgnore
    public String getPassword() {
        return pwd;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.pwd = password;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Dev{" +
                "id=" + id +
                ", devName='" + devName + '\'' +
                ", userName='" + userName + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
