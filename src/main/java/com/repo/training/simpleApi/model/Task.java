package com.repo.training.simpleApi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Tasks")
public class Task implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Title must not be null")
    @Length(min = 2, message = "Title must have more than 2 chars")
    @Length(max = 15, message = "Title must have less than 15 chars")
    private String title;

    @NotNull(message = "Description must not be null")
    @Length(min = 5, message = "Description must have more than 5 chars")
    @Length(max = 200, message = "Description must have less than 200 chars")
    private String description;

    private Date dueDate;

    private Boolean done;

    @ManyToMany(mappedBy = "tasks", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Dev> devs = new ArrayList<>();

    public Task() {
    }

    public Task(@NotNull String title, @NotNull String description, @Valid Date dueDate, Boolean done) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.done = done;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public List<Dev> getDevs() {
        return devs;
    }

    public void setDevs(List<Dev> devs) {
        this.devs = devs;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dueDate=" + dueDate +
                ", done=" + done +
                '}';
    }
}
