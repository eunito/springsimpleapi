package com.repo.training.simpleApi.responses.tasks;

import com.repo.training.simpleApi.model.Task;

import java.util.List;

public class TasksListSuccess {
    public Boolean success;
    public List<Task> data;

    // Custom handler for a regular dev endpoint sucess containing a list
    public TasksListSuccess(Boolean success, List<Task> data) {
        this.success = success;
        this.data = data;
    }

}