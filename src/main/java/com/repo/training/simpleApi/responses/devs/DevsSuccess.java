package com.repo.training.simpleApi.responses.devs;

import com.repo.training.simpleApi.model.Dev;

public class DevsSuccess {
    public Boolean sucess;
    public Dev data;

    // Custom handler for a regular dev endpoint success containing only one entity
    public DevsSuccess(Boolean sucess, Dev dev) {
        this.sucess = sucess;
        this.data = dev;
    }
}