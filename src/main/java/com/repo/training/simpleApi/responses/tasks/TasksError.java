package com.repo.training.simpleApi.responses.tasks;

public class TasksError {
    public Boolean sucess;
    public String message;

    // Custom handler for a regular dev endpoint error
    public TasksError(Boolean sucess, String message) {
        this.sucess = sucess;
        this.message = message;
    }

}