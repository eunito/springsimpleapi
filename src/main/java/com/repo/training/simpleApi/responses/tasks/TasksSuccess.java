package com.repo.training.simpleApi.responses.tasks;

import com.repo.training.simpleApi.model.Task;

public class TasksSuccess {
    public Boolean sucess;
    public Task data;

    // Custom handler for a regular dev endpoint success containing only one entity
    public TasksSuccess(Boolean sucess, Task task) {
        this.sucess = sucess;
        this.data = task;
    }
}