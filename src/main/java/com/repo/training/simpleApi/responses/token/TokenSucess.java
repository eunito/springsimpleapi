package com.repo.training.simpleApi.responses.token;

import com.repo.training.simpleApi.model.JwtResponse;

public class TokenSucess {
    public Boolean sucess;
    public String token;

    // Custom handler for a regular authenticate endpoint success
    public TokenSucess(Boolean sucess, JwtResponse token) {
        this.sucess = sucess;
        this.token = token.getToken();
    }
}
