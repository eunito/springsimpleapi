package com.repo.training.simpleApi.responses.devs;

import com.repo.training.simpleApi.model.Dev;

import java.util.List;

public class DevsListSuccess {
    public Boolean success;
    public List<Dev> data;

    // Custom handler for a regular dev endpoint sucess containing a list
    public DevsListSuccess(Boolean success, List<Dev> data) {
        this.success = success;
        this.data = data;
    }

}