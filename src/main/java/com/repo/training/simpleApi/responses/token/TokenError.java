package com.repo.training.simpleApi.responses.token;

public class TokenError {
    public Boolean success;
    public String message;

    // Custom handler for a regular authenticate endpoint error
    public TokenError(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

}