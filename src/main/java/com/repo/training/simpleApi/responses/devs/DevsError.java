package com.repo.training.simpleApi.responses.devs;

public class DevsError {
    public Boolean sucess;
    public String message;

    // Custom handler for a regular dev endpoint error
    public DevsError(Boolean sucess, String message) {
        this.sucess = sucess;
        this.message = message;
    }

}