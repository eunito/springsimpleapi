package com.repo.training.simpleApi.service;

import com.repo.training.simpleApi.exception.TaskNotFoundException;
import com.repo.training.simpleApi.model.Task;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public interface TaskService {
    public List<Task> getAllTasks();
    public Task createTask(Task task);
    public Task getTaskById(Long taskId) throws TaskNotFoundException;
    public Task updateTask(Long taskId,Task taskDetails) throws TaskNotFoundException;
    public ResponseEntity<?> deleteTask(Long taskId) throws TaskNotFoundException;
}