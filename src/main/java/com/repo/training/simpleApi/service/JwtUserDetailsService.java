package com.repo.training.simpleApi.service;

import com.repo.training.simpleApi.model.Dev;
import com.repo.training.simpleApi.repository.DevRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private DevRepository devRepository;
    /**
     * Obtains the user details based on a given username
     * @param username - the username to get data from
     * @return {User} - a user for API usage
     * @throws UsernameNotFoundException - if an error occurred in the search process
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Dev dev = devRepository.findByUserName(username);
        if (dev == null ) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));

        return new org.springframework.security.core.userdetails.User(dev.getUserName(), dev.getPassword(), authorities);
    }
}