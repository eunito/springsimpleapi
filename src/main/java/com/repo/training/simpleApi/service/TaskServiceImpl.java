package com.repo.training.simpleApi.service;

import com.repo.training.simpleApi.exception.TaskNotFoundException;
import com.repo.training.simpleApi.model.Task;
import com.repo.training.simpleApi.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class TaskServiceImpl implements TaskService {

    // "Connect" to the Repository "containing" the hibernate jpa methods
    @Autowired
    private TaskRepository taskRepository;

    // "Connect" to the message bundle source bean
    @Autowired
    private MessageSource messageSource;

    private static final Logger LOGGER= LoggerFactory.getLogger(TaskServiceImpl.class);

    /**
     * Get All Tasks
     * @return {List<Task>} - the list of tasks
     */
    @Override
    public List<Task> getAllTasks() {
        return (List<Task>) taskRepository.findAll();
    }

    /**
     * Create a new Task
     * @param task - the new task to become created
     * @return {Task} - the created task
     */
    @Override
    public Task createTask(Task task) {
        if (task == null) {
            LOGGER.error("Invalid params received");
            throw new Error("TASK_NULL");
        }
        return taskRepository.save(task);
    }

    /**
     * Gets a task by it's id
     * @param taskId - the task id
     * @return {Task} - the desired task to get data from
     * @throws TaskNotFoundException
     */
    @Override
    public Task getTaskById(Long taskId) throws TaskNotFoundException {
        if (taskId == null || taskId < 0) {
            LOGGER.error("Invalid params received");
            throw new Error("INVALID_ID");
        }

        return taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId, messageSource));
    }

    /**
     * Updates a Task
     * @param taskId - the task id for update
     * @param taskDetails - the updated details
     * @return {Task} - the updated task
     * @throws TaskNotFoundException
     */
    @Override
    public Task updateTask(Long taskId,Task taskDetails) throws TaskNotFoundException {
        if (taskId == null || taskId < 0 || taskDetails == null) {
            LOGGER.error("Invalid params received");
            throw new Error("UPDATE_PARAMS_ERROR");
        }
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId, messageSource));

        task.setTitle(taskDetails.getTitle());
        task.setDescription(taskDetails.getDescription());
        task.setDueDate(taskDetails.getDueDate());
        task.setDone(taskDetails.getDone());

        Task updatedTask = taskRepository.save(task);
        LOGGER.info("Updated task: "+updatedTask.toString());
        return updatedTask;
    }

    /**
     * Removes a task from DB
     * @param taskId - the id of the task to become removed
     * @return {ResponseEntity<>} - a response object based on the operation
     * @throws TaskNotFoundException
     */
    @Override
    public ResponseEntity<?> deleteTask(Long taskId) throws TaskNotFoundException {
        if (taskId == null || taskId < 0) {
            LOGGER.error("Invalid params received");
            throw new Error("INVALID_ID");
        }
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId, messageSource));

        taskRepository.delete(task);
        LOGGER.info("Removed task with id: "+taskId);
        return ResponseEntity.ok().build();
    }

}
