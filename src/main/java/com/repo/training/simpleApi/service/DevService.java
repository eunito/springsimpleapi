package com.repo.training.simpleApi.service;

import com.repo.training.simpleApi.exception.DevNotFoundException;
import com.repo.training.simpleApi.model.Dev;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public interface DevService {
    public List<Dev> getAllDevs();
    public Boolean createDev(Dev dev, BCryptPasswordEncoder bCryptPasswordEncoder);
    public Dev getDevById(Long devId) throws DevNotFoundException;
    public Dev updateDev(Long devId,Dev devDetails, BCryptPasswordEncoder bCryptPasswordEncoder) throws DevNotFoundException;
    public ResponseEntity<?> deleteDev(Long devId) throws DevNotFoundException;
}
