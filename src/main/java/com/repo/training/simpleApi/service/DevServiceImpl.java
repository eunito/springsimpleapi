package com.repo.training.simpleApi.service;

import com.repo.training.simpleApi.exception.DevNotFoundException;
import com.repo.training.simpleApi.model.Dev;
import com.repo.training.simpleApi.repository.DevRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class DevServiceImpl implements DevService {

    // "Connect" to the Repository "containing" the hibernate jpa methods
    @Autowired
    private DevRepository devRepository;

    // "Connect" to the message bundle source bean
    @Autowired
    private MessageSource messageSource;

    @Override
    public List<Dev> getAllDevs() {
        return (List<Dev>) devRepository.findAll();
    }

    private static final Logger LOGGER= LoggerFactory.getLogger(DevServiceImpl.class);

    /**
     * Creates a new dev
     * @param {Dev} dev - the dev object with name and password
     * @param {BCryptPasswordEncoder} bCryptPasswordEncoder - the password encoder
     * @return {Boolean} - true if was succcessfull
     * @throws Exception if error occurred
     */
    @Override
    public Boolean createDev(Dev dev, BCryptPasswordEncoder bCryptPasswordEncoder) {
        try {
            if(dev == null) {
                LOGGER.error("Invalid params received");
                throw new Error("DEV_NULL");
            }

            String name = dev.getName();
            String username = dev.getUserName();
            // Encrypt the dev's password
            String pwd = bCryptPasswordEncoder.encode(dev.getPassword());

            devRepository.saveDev(name, username, pwd);
            LOGGER.info("Saved dev: "+dev.toString());
        } catch (Exception e) {
            LOGGER.error("Saved dev: "+dev.toString() + ". Detail: " + e.getMessage());
            throw new Error("Error creating a new dev.");
        }
        return true;
    }

    /**
     * Gets a dev's details by id
     * @param devId - the dev id for detail obtaining
     * @return {Dev} - the dev
     * @throws DevNotFoundException
     */
    @Override
    public Dev getDevById(Long devId) throws DevNotFoundException {
        if (devId == null || devId < 0) {
            LOGGER.error("Invalid params received");
            throw new Error("INVALID_ID");
        }

        // Prettier but not log friendly
        return devRepository.findById(devId)
                .orElseThrow(() -> new DevNotFoundException(devId, messageSource));
    }

    /**
     * Updates a dev information
     * @param devId - the dev id
     * @param devDetails - the updated details
     * @param {BCryptPasswordEncoder} bCryptPasswordEncoder - the password encoder
     * @return {Dev} - the updated dev
     * @throws DevNotFoundException
     */
    @Override
    public Dev updateDev(Long devId, Dev devDetails, BCryptPasswordEncoder bCryptPasswordEncoder) throws DevNotFoundException {
        if (devId == null || devId < 0 || devDetails == null) {
            LOGGER.error("Invalid params received");
            throw new Error("UPDATE_PARAMS_ERROR");
        }

        Dev dev = devRepository.findById(devId)
                .orElseThrow(() -> new DevNotFoundException(devId, messageSource));

        dev.setName(devDetails.getName());
        dev.setUserName(devDetails.getUserName());
        // Encrypt the dev's password
        dev.setPassword(bCryptPasswordEncoder.encode(devDetails.getPassword()));
        LOGGER.info("Updated dev info: "+dev.toString());
        return devRepository.save(dev);
    }

    /**
     * Removes a dev from DB
     * @param devId - the dev id for removal
     * @return {ResponseEntity<>} - a response object based on the operation
     * @throws DevNotFoundException
     */
    @Override
    public ResponseEntity<?> deleteDev(Long devId) throws DevNotFoundException {
        if (devId == null || devId < 0) {
            LOGGER.error("Invalid params received");
            throw new Error("INVALID_ID");
        }

        Dev dev = devRepository.findById(devId)
                .orElseThrow(() -> new DevNotFoundException(devId, messageSource));

        devRepository.delete(dev);
        LOGGER.info("Deleted dev with id: "+devId);
        return ResponseEntity.ok().build();
    }
}
