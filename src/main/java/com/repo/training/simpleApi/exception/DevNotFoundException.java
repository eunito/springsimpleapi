package com.repo.training.simpleApi.exception;

import org.springframework.context.MessageSource;
import java.util.Locale;

public class DevNotFoundException extends Exception {
    private static String DEV_NOTFOUND_ERROR = "exception.dev.notfound";

    /**
     * Sets the dev not found message based on the i18n file example
     * @param devId - the id of the occurrence
     * @param messageSource - the message bundle source
     */
    public DevNotFoundException(long devId, MessageSource messageSource) {
        super(String.format("%s : '%s'", messageSource.getMessage(DEV_NOTFOUND_ERROR, null, Locale.getDefault()), devId));
    }
}
