package com.repo.training.simpleApi.exception;

import org.springframework.context.MessageSource;
import java.util.Locale;

public class TaskNotFoundException extends Exception {
    private static String TASK_NOTFOUND_ERROR = "exception.task.notfound";

    /**
     * Sets the task not found message based on the i18n file example
     * @param taskId - the id of the occurrence
     * @param messageSource - the message bundle source
     */
    public TaskNotFoundException(long taskId, MessageSource messageSource) {
        super(String.format("%s : '%s'", messageSource.getMessage(TASK_NOTFOUND_ERROR, null, Locale.getDefault()), taskId));
    }
}