package com.repo.training.simpleApi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SimpleApiApplication {
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	private static final Logger LOGGER= LoggerFactory.getLogger(SimpleApiApplication.class);

	public static void main(String[] args) {

		SpringApplication.run(SimpleApiApplication.class, args);
		LOGGER.info("App is running!");
	}

}