package com.repo.training.simpleApi.controller;

import com.repo.training.simpleApi.model.JwtRequest;
import com.repo.training.simpleApi.model.JwtResponse;
import com.repo.training.simpleApi.responses.token.TokenError;
import com.repo.training.simpleApi.responses.token.TokenSucess;
import com.repo.training.simpleApi.security.JwtTokenUtil;
import com.repo.training.simpleApi.service.JwtUserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@Api(value = "/authenticate", description = "Token route - required for api operations", tags = { "/authenticate" })
@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationController.class);

    /**
     * Creates an authentication token for a user based on a post request to the authenticate route
     * @param authenticationRequest - the auth request
     * @return {Object} - {success: true/false, token: the token}
     * @throws Exception - if an error occurred in the token creation process.
     */
    @Operation(summary = "Get token for operations")
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        try {
            LOGGER.info("Received a request to generate a token for user: "+authenticationRequest.getUsername());

            final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
            Boolean validCredentials = authenticate(userDetails, authenticationRequest.getPassword());

            if (!validCredentials) {
                TokenError tokenErrorResp = new TokenError(false, "Error generating token.");
                return new ResponseEntity<TokenError>(tokenErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            final String token = jwtTokenUtil.generateToken(userDetails);
            TokenSucess tokenSuccessResponse = new TokenSucess(true, new JwtResponse(token));
            LOGGER.info("Obtained token with success ");
            return new ResponseEntity<TokenSucess>(tokenSuccessResponse, HttpStatus.OK);
        } catch (Exception e) {
            TokenError tokenErrorResp = new TokenError(false, "Error generating token.");
            LOGGER.error("Error generating a token. Details: "+ e);
            return new ResponseEntity<TokenError>(tokenErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Authenticates a user based on the Spring Authentication Manager
     * @param userDetails - the user's details object for validation purposes
     * @param password - the user's password
     * @throws Exception - if an error occurred in the authentication process.
     */
    private Boolean authenticate(UserDetails userDetails, String password) {
        try {
            LOGGER.info("Received a request to authenticate the user: " + userDetails.getUsername());

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
            authenticationManager.authenticate(usernamePasswordAuthenticationToken);

            return true;
        } catch (DisabledException e) {
            LOGGER.error("User is disabled. Details: " + e.getMessage());
            return false;
        } catch (BadCredentialsException e) {
            LOGGER.error("Invalid credentials. Details: " + e.getMessage());
            return false;
        } catch (Exception e) {
            LOGGER.error("Error authenticating user. Details: " + ExceptionUtils.getStackTrace(e));
            return false;
        }
    }
}