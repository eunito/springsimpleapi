package com.repo.training.simpleApi.controller;

import com.repo.training.simpleApi.exception.DevNotFoundException;
import com.repo.training.simpleApi.model.Dev;
import com.repo.training.simpleApi.responses.devs.DevsError;
import com.repo.training.simpleApi.responses.devs.DevsListSuccess;
import com.repo.training.simpleApi.responses.devs.DevsSuccess;
import com.repo.training.simpleApi.service.DevService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "/devs", description = "Operations about devs", tags = { "/devs" })
@RestController
@RequestMapping(value = "/")
public class DevController {

    /////////////////////////////////////////////////////////////////////////////////////////
    //    In this controller one can verify the devs endpoints handling.
    //    All the responses are managed via custom handlers in the responses/devs package.
    //    Also one can verify that the requests go through the service implementation,
    //    rather than using the repository db layer directly.
    /////////////////////////////////////////////////////////////////////////////////////////

    private final DevService devService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final Logger LOGGER = LoggerFactory.getLogger(DevController.class);

    public DevController(DevService devService,
                         BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.devService = devService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Get All Devs
     * Handles GET requests to the /devs endpoint
     * @return {Object} - {success: true/false, data: the devs list / error message}
     */
    @Operation(summary = "Get all devs")
    @GetMapping(value = "/devs", produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> getAllTDevs() {
        try {
                LOGGER.info("Received a request to list all devs.");
                List<Dev> devs = devService.getAllDevs();

                DevsListSuccess devsListResponse = new DevsListSuccess(true, devs);
                LOGGER.info("Obtained devs list with response: "+ devsListResponse.toString());
                return new ResponseEntity<DevsListSuccess>(devsListResponse, HttpStatus.OK);
            } catch (Error e) {
                DevsError devsListResponse = new DevsError(false, e.getMessage());
                LOGGER.error("Error obtaining devs list. Details: "+ e.getMessage());
                return new ResponseEntity<DevsError>(devsListResponse, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }

    /**
     * Creates a new Dev
     * Handles POST requests to the /devs endpoint
     * @param {Dev} - the dev to insert
     * @return {Object} - {success: true/false, data: the created data / error message}
     */
    @Operation(summary = "Create a new dev")
    @PostMapping(value = "/devs", consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> createDev(@Valid @RequestBody Dev dev) {
        try {
            if ( dev == null) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }
            LOGGER.info("Received a new dev creation order: "+ dev.toString());

            // Save that new dev
            devService.createDev(dev, bCryptPasswordEncoder);

            DevsSuccess devSuccessResp = new DevsSuccess(true, dev);
            LOGGER.info("Created a new dev. Response: "+ devSuccessResp.toString());
            return new ResponseEntity<DevsSuccess>(devSuccessResp, HttpStatus.OK);
        } catch (Error e) {
            DevsError devsErrorResp = new DevsError(false, e.getMessage());
            LOGGER.error("Error creating a new dev. Details: "+ e.getMessage());
            return new ResponseEntity<DevsError>(devsErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a Single Dev by id
     * Handles GET requests to the /devs/{id} endpoint
     * @param {Long} - the id of the dev to get info from
     * @return {Object} - {success: true/false, data: the updated data / error message}
     */
    @Operation(summary = "Get a dev by id")
    @GetMapping(value = "/devs/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> getDevById(@PathVariable(value = "id") Long devId) {
        try {
            if ( devId == null || devId < 0) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a new request to get dev's "+ devId + " details. ");
            Dev dev = devService.getDevById(devId);
            LOGGER.info("Obtained dev's "+ devId + " detail: "+ dev.toString());

            DevsSuccess devSuccessResp = new DevsSuccess(true, dev);
            LOGGER.info("Obtained dev's "+ devId + " detail. Response: "+ devSuccessResp.toString());
            return new ResponseEntity<DevsSuccess>(devSuccessResp, HttpStatus.OK);
        } catch (Error | DevNotFoundException e) {
            DevsError devsErrorResp = new DevsError(false, e.getMessage());
            LOGGER.error("Error getting a dev's "+ devId + " detail. Details: "+ e.getMessage());
            return new ResponseEntity<DevsError>(devsErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update a Dev
     * Handles PUT requests to the /devs/{id} endpoint
     * @param devId - the id of the dev
     * @param devDetails - the details given for update
     * @return {Object} - {success: true/false, data: the updated data / error message}
     * @throws JSONException
     */
    @Operation(summary = "Update a dev by id")
    @PutMapping(value = "/devs/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> updateDev(@PathVariable(value = "id") Long devId, @Valid @RequestBody Dev devDetails) {
        try {
            if ( devId == null || devId < 0 || devDetails == null) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a new request to update dev's "+ devId + " details: " + devDetails.toString());
            Dev updatedDev = devService.updateDev(devId, devDetails, bCryptPasswordEncoder);
            LOGGER.info("Updated dev's "+ devId + " info to: "+ updatedDev.toString());

            DevsSuccess devSuccessResp = new DevsSuccess(true, updatedDev);
            LOGGER.info("Updated dev's "+ devId + " info to: "+ updatedDev.toString() + ". Response: "+devSuccessResp.toString());
            return new ResponseEntity<DevsSuccess>(devSuccessResp, HttpStatus.OK);
        } catch (Error | DevNotFoundException e) {
            DevsError devsErrorResp = new DevsError(false, e.getMessage());
            LOGGER.error("Error updating dev's "+ devId + " info. Details: "+ e.getMessage());
            return new ResponseEntity<DevsError>(devsErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a Dev from db
     * Handles DELETE requests to the /devs/{id} endpoint
     * @param devId - the dev id
     * @return {Object} - {success: true/false, data: '<messageIfFalse>'}
     * @throws JSONException
     */
    @Operation(summary = "Remove a dev based on id")
    @DeleteMapping(value = "/devs/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> deleteDev(@PathVariable(value = "id") Long devId) throws JSONException {
        try {
            if ( devId == null || devId < 0) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a new request to remove dev "+ devId);
            devService.deleteDev(devId);
            DevsSuccess devSuccessResp = new DevsSuccess(true, null);
            LOGGER.info("Updated dev's "+ devId + " info. Response: "+devSuccessResp.toString());
            return new ResponseEntity<DevsSuccess>(devSuccessResp, HttpStatus.OK);
        } catch (Error | DevNotFoundException e) {
            DevsError devsErrorResp = new DevsError(false, e.getMessage());
            LOGGER.error("Error removing dev's "+ devId + " info. Details: "+ e.getMessage());
            return new ResponseEntity<DevsError>(devsErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}