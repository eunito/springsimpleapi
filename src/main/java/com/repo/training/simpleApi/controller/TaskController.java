package com.repo.training.simpleApi.controller;

import com.repo.training.simpleApi.exception.TaskNotFoundException;
import com.repo.training.simpleApi.model.Task;
import com.repo.training.simpleApi.responses.tasks.TasksError;
import com.repo.training.simpleApi.responses.tasks.TasksListSuccess;
import com.repo.training.simpleApi.responses.tasks.TasksSuccess;
import com.repo.training.simpleApi.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "/tasks", description = "Operations about tasks", tags = { "/tasks" })
@RestController
@RequestMapping(value = "/")
public class TaskController {

    /////////////////////////////////////////////////////////////////////////////////////////
    //    In this controller one can verify the tasks endpoints handling.
    //    All the responses are managed via custom handlers in the responses/tasks package.
    //    Also one can verify that the requests go through the service implementation,
    //    rather than using the repository db layer directly.
    /////////////////////////////////////////////////////////////////////////////////////////

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    private static final Logger LOGGER= LoggerFactory.getLogger(TaskController.class);

    /**
     * Get All Tasks
     * Handles GET requests to the /tasks endpoint
     * @return {Object} - {success: true/false, data: the tasks list / error message}
     */
    @Operation(summary = "Get all tasks")
    @GetMapping("/tasks")
    public ResponseEntity<?> getAllTasks() {
        try {
        LOGGER.info("Received a request to list all tasks.");
            List<Task> tasks = taskService.getAllTasks();

            TasksListSuccess tasksListResponse = new TasksListSuccess(true, tasks);
            LOGGER.info("Obtained tasks list with response: "+ tasksListResponse.toString());
            return new ResponseEntity<TasksListSuccess>(tasksListResponse, HttpStatus.OK);
        } catch (Error e) {
            TasksError tasksListResponse = new TasksError(false, e.getMessage());
            LOGGER.error("Error obtaining tasks list. Details: "+ e.getMessage());
            return new ResponseEntity<TasksError>(tasksListResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Creates a new Task
     * Handles POST requests to the /tasks endpoint
     * @param {Task} - the task to insert
     * param.title
     * param.description
     * param.dueDate
     * param.done
     * @return {Object} - {success: true/false, data: the created data / error message}
     */
    @Operation(summary = "Create a new task")
    @PostMapping("/tasks")
    public ResponseEntity<?> createTask(@Valid @RequestBody Task task) {
        try {
            if ( task == null) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a request to create a new task: "+task.toString());
            taskService.createTask(task);
            TasksSuccess taskSuccessResp = new TasksSuccess(true, task);
            LOGGER.info("Created a new task. Response: "+ taskSuccessResp.toString());
            return new ResponseEntity<TasksSuccess>(taskSuccessResp, HttpStatus.OK);
        } catch (Error e) {
            TasksError tasksErrorResp = new TasksError(false, e.getMessage());
            LOGGER.error("Error creating a new task. Details: "+ e.getMessage());
            return new ResponseEntity<TasksError>(tasksErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a Single Task
     * Handles GET requests to the /tasks/{id} endpoint
     * @param taskId - the task id to search for
     * @return {Object} - {success: true/false, data: the updated data / error message}
     * @throws TaskNotFoundException
     */
    @Operation(summary = "Get a task by id")
    @GetMapping("/tasks/{id}")
    public ResponseEntity<?> getTaskById(@PathVariable(value = "id") Long taskId) throws TaskNotFoundException {
        try {
            if ( taskId == null || taskId < 0) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a request to get task with id: "+taskId);
            Task task = taskService.getTaskById(taskId);
            LOGGER.info("Obtained task's "+ taskId + " detail: "+ task.toString());

            TasksSuccess taskSuccessResp = new TasksSuccess(true, task);
            LOGGER.info("Obtained task's "+ taskId + " detail. Response: "+ taskSuccessResp.toString());
            return new ResponseEntity<TasksSuccess>(taskSuccessResp, HttpStatus.OK);
        } catch (Error | TaskNotFoundException e) {
            TasksError tasksErrorResp = new TasksError(false, e.getMessage());
            LOGGER.error("Error getting task's "+ taskId + " detail. Details: "+ e);
            return new ResponseEntity<TasksError>(tasksErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update a Task
     * Handles PUT requests to the /tasks/{id} endpoint
     * @param taskId - the task id to edit
     * @param taskDetails - the updated task details
     * @return {Object} - {success: true/false, data: the updated data / error message}
     * @throws TaskNotFoundException
     */
    @Operation(summary = "Update a task by id")
    @PutMapping("/tasks/{id}")
    public ResponseEntity<?> updateTask(@PathVariable(value = "id") Long taskId,
                           @Valid @RequestBody Task taskDetails) throws TaskNotFoundException {
        try {
            if ( taskId == null || taskId < 0 || taskDetails == null) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a request to update the task with id: "+taskId + " with "+ taskDetails.toString());
            Task updatedTask = taskService.updateTask(taskId, taskDetails);
            LOGGER.info("Updated task's "+ taskId + " info to: "+ updatedTask.toString());

            TasksSuccess taskSuccessResp = new TasksSuccess(true, updatedTask);
            LOGGER.info("Updated task's "+ taskId + " info to: "+ updatedTask.toString() + ". Response: "+taskSuccessResp.toString());
            return new ResponseEntity<TasksSuccess>(taskSuccessResp, HttpStatus.OK);
        } catch (Error | TaskNotFoundException e) {
            TasksError tasksErrorResp = new TasksError(false, e.getMessage());
            LOGGER.error("Error updating task's "+ taskId + " info. Details: "+ e.getMessage());
            return new ResponseEntity<TasksError>(tasksErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes a Task
     * Handles DELETE requests to the /tasks/{id} endpoint
     * @param taskId - the task to become deleted
     * @return {Object} - {success: true/false, data: '<messageIfFalse>'}
     * @throws TaskNotFoundException
     */
    @Operation(summary = "Remove a task by id")
    @DeleteMapping("/tasks/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable(value = "id") Long taskId) throws TaskNotFoundException {
        try {
            if (taskId == null || taskId < 0) {
                LOGGER.error("Invalid params received");
                throw new Error("INVALID_PARAMS");
            }

            LOGGER.info("Received a request to delete the task with id: "+taskId);
            taskService.deleteTask(taskId);
            TasksSuccess taskSuccessResp = new TasksSuccess(true, null);
            LOGGER.info("Updated task's "+ taskId + " info. Response: "+taskSuccessResp.toString());
            return new ResponseEntity<TasksSuccess>(taskSuccessResp, HttpStatus.OK);
        } catch (Error | TaskNotFoundException e) {
            TasksError tasksErrorResp = new TasksError(false, e.getMessage());
            LOGGER.error("Error removing task's "+ taskId + " info. Details: "+ e.getMessage());
            return new ResponseEntity<TasksError>(tasksErrorResp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}