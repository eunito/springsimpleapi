package com.repo.training.simpleApi.repository;

import com.repo.training.simpleApi.model.Dev;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

// Layer between the dev service implementation and the DB
public interface DevRepository extends CrudRepository<Dev, Long> {

    // Uses hibernate to get data by column name using the given param
    List<Dev> findByDevNameContaining(String name);
    Dev findByUserName(String username);

    // In the following example one can visualize a native query instead of using hibernate methods
    // Transactional and Modifying annotations required here due to the fact it is an insert
    @Transactional
    @Modifying
    @Query(value = "insert into devs (dev_name, user_name, pwd) VALUES (?1, ?2, ?3)", nativeQuery = true)
    void saveDev(String name, String userName, String password);
}
