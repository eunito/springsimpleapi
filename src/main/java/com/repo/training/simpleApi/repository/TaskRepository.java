package com.repo.training.simpleApi.repository;

import com.repo.training.simpleApi.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

// Layer between the task service implementation and the DB
public interface TaskRepository extends CrudRepository<Task, Long> {
    // Finds tasks by title containing the given string
    List<Task> findByTitleContaining(String title);
}
