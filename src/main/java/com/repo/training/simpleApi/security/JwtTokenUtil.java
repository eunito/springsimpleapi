package com.repo.training.simpleApi.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    @Value("${jwt.token.validity}")
    private Long JWT_TOKEN_VALIDITY;

    @Value("${jwt.secret}")
    private String secret;

    /**
     * Retrieve username from jwt token
     * @param {String} token - the token to get data from
     * @return {String} the username
     */
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    /**
     * Retrieve expiration date from jwt token
     * @param {String} token - the token to get data from
     * @return {String} the expiration date
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * Retrieve claims from jwt token
     * @param {String} token - the token to get data from
     * @param {Object} claimsResolver - the claims resolver
     * @return {Object} the claims
     */
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     * For retrieving any information from token we will need the secret key
     * @param {String} token - the token to get data from
     * @return {Object} the token claims
     */
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    /**
     * Checks if the token has expired
     * @param {String} token - the token to get data from
     * @return {Boolean} true/false according if the token has expired or not
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * Generates token for user
     * @param userDetails - the user details for token generation
     * @return {String} the user's token
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    /**
     * Auxiliary method to define token claims: Issuer, Expiration, Subject, and the ID, ...
     * Signs the JWT using HS512 based on the stored secret key in the application properties
     * NOTE: JWT to a URL-safe string process based on JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
     * @param claims  - the token claims to be used
     * @param subject - the subject to whom the token belongs to (https://medium.com/tableless/entendendo-tokens-jwt-json-web-token-413c6d1397f6)
     * @return {String} the user's token
     */
    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    /**
     * Validates token confronting it's info against provider userName and token expiry date
     * @param {String} token - the token to get data from
     * @param {Object} userDetails - the user details for token generation
     * @return {Boolena} true/false according to the token validity
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}