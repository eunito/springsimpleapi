# Simple API

Simple API is as it's name refers, an API for devs and their tasks CRUD operations.
The main idea for this API was to recycle know how in JAVA / Spring and main logic implementation in certain scenarios.
It exposes two endpoints groups: /devs and /users for those purpose.


# Features

- Create/delete/update/get devs details
- Create/delete/update/get tasks details
- Create/delete dev's tasks associations (many-to-many relation)
- Localization example using i18n resource bundle (only applied in one scenario for usage demonstration)
- Dev's password encryption
- Custom API replies with error / exception handling
- Postgres DB schema based instead of hibernate schema creation
- Documented methods using JSDocs
- Postman routes for ease of use in requests (@ /others folder)
- Swagger documented routes - http://localhost:8080/swagger-ui/index.html
- Logger implementation (logs to console and file @ /logs folder)

### Deployment
The API can be executed via IDE executing:
```sh
$ mvn spring-boot:run
```

The API deployment can also be done via jar or war files. For this project jar was defined in the pom.xml file for this matter.
Note: all the configurations used in the created jar will be the ones based on the apllication configs;

Open the project directory and execute a maven clean to remove all the entries in the /target folder;
```sh
$ cd <project_folder>
$ mvn clean
```
Execute a maven install to remove all the entries in the /target folder. 
This will compile all classes, existing tests, configs and bundles them with the dependencies jars files into a large .jar file;
```sh
$ mvn install
```
Note: the previous two commands can be executed in one: 
```sh
$ mvn clean install
```


Open the /target directory and copy the created jar to the destination folder where that jar, containing the full simple API can be called/executed; 

The simple API can now be executed via terminal using
```sh
$ cd <folder_containing_jar>
$ java -jar <jar_filename>.jar 
```

### Todo list - improvements

- Create an example route for an external api usage and data storage in DB
- JUnit testing
- Containerize the App with Docker (using a container for Postgres)
